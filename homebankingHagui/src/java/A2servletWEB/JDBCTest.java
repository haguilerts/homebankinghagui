
package A2servletWEB; //http://localhost:8118/phpmyadmin/

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCTest {
    public static void main(String[] args) throws SQLException {
        System.out.println("[...]");
        consultar();
        insertar();
        consultar();
        System.out.println("[OK]");
    }
    public static void consultar() {
        System.out.println("[...]");
        final String consulta = "SELECT * FROM productos";// resalizo la consulta SQL
        try {
            Connection myCon = DriverManager.getConnection(
                    //se conecta a la base de datos
                            "jdbc:mysql://10.0.0.191:3306/alumnos",
                            "usuario",
                            "clave");
            PreparedStatement pstm = myCon.prepareStatement(consulta);// prepara la consulta
            ResultSet rs = pstm.executeQuery();//envia y guarda el resultado
            while (rs.next()) { //recorre el array (resultado de la base de datos)
                // imprime al primero en la lista y toma e valor precio
                System.out.println("Producto: " + rs.getString(1) + " Precio: " + rs.getString(2));

            }

        } catch (SQLException ex) {
            System.out.println("Alerta: " + ex.getMessage());
        }
        System.out.println("[OK]");
    }
    public static void insertar() {
        System.out.println("[...]");
        String consulta2 = "INSERT INTO productos(nombre,precio) VALUES('Motorola','11000')";
        try {
            Connection myCon  = DriverManager.getConnection(
                            "jdbc:mysql://10.0.0.191:3306/alumnos",
                            "usuario",
                            "clave");
            PreparedStatement pstm = myCon.prepareStatement(consulta2);
            pstm.executeQuery();

        } catch (SQLException ex) {
            System.out.println("Alerta: " + ex.getMessage());
        }

        System.out.println("[OK]");
    }

}
