/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A2servletWEB;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author haguilerts
 */
@WebServlet(name = "A1ServletBD", urlPatterns = {"/A1ServletBD"})
public class A1ServletBD extends HttpServlet {
     Gson convertir = new Gson();
    @Override
    //////////********* dOGET *********//////////
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                   
            
         try {
             String consulta = "SELECT * FROM productos;";
             Connection con = ConeccionBD.getInstance().getConnection(); // se conecta
             PreparedStatement pstm = con.prepareStatement(consulta); // prepara la consulta 
             ResultSet rs = pstm.executeQuery(); //envia y guarda el resultado
         while (rs.next()) { //consulta si hay un nuevo registro. 
                // imprime al primero en la lista y toma e valor saldo
                System.out.println(rs.getString(1) + " - " + rs.getString("precio")); 
                
            }
         } catch (ClassNotFoundException ex) {
             System.out.println("Alerta: " + ex.getMessage());
         } catch (SQLException ex) {
             System.out.println("Alerta: " + ex.getMessage());
        }   
        
        System.out.println("Capturamos el llamado del cliente por metodo GET"); 
        resp.getWriter().println("Felicitaciones, este es el registro de Giovanny.");
        
    }
    //////////********* dOPost *********//////////
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
    }

  
    
     
}

    
