/**    Created on : 13/10/2019, 18:245:50    Author     : haguilerts **/
class Array{
    static baseDatos(){
        let cuentas = [
            {usuario: "gio",    password1: "1",     edad: 11  },
            {usuario: "gio",    password1: "12",    edad: 21 },
            {usuario: "gio",    password1: "123",   edad: 31 },
            {usuario: "marco",  password: "123m",   edad: 42 },
            {usuario: "sergio", password: "123s",   edad: 52 },
            {usuario: "natu",   password: "123n",   edad: 62 },
            {usuario: "efi",    password: "123e",   edad: 10 }
            ];
        return cuentas;
    }
    
    static Map(){
        console.log("provando metodo map ");

        //------------  array tradicional---------------
//        let  datoCuenta = Ejecutable.baseDatos();// lo traigo de la BBDD en formato array
//        let usuarioCuentaGuardado= []; // añado una nueva variable vacio para usuarios. 
//        let passCuentaGuardado= []; // añado una nueva variable vacio para paswords.
//            // recorro toda la longitud con el for imprimiedo solo los usuarios.
//        for(let i=0; i<datoCuenta.length; i++){
//            usuarioCuentaGuardado.push(datoCuenta[i].usuario);// añado todo los "usuarios:" de "datoCuenta" al "datoCuentaGuardado" del tipo array que estaba vacio.
//            passCuentaGuardado.push(datoCuenta[i].password);// añado todo los "Pasword:" de "datoCuenta" al "datoCuentaGuardado" del tipo array que estaba vacio.
//            
//        }
//                console.log("Usuarios:");
//            console.log(usuarioCuentaGuardado); // muestra los usuarios del array.
//                console.log("Password:");
//            console.log(passCuentaGuardado); // muestra los Pasword del array.
//        // datoCuenta.map(usuarioPorUsuario => console.log());

        //------------ otra forma de hacer el array (usando metodo)---------------
        let  datoCuenta = Pruevas.baseDatos();// lo trae todo, usuario y password de la BBDD en formato array
        let miDatoUsuarioArry = datoCuenta.map(function (datoUsuario) {
            //console.log(dato.password);
            return datoUsuario.usuario; // retorna la contraseña, este metodo funciona como un GET y SET..
        });
        let miDatoPassArry = datoCuenta.map(function (datoPass) {
            //console.log(dato.password);
            return datoPass.password; // retorna la contraseña, este metodo funciona como un GET y SET..
        });
        console.log("Usuarios:");
        console.log(miDatoUsuarioArry);
        console.log("password:");
        console.log(miDatoPassArry);
    }
    
    static Filter(){
        console.log("provando metodo filter...");
        //------------  array tradicional---------------
//        let  datoCuenta = Pruevas.baseDatos();// lo trae todo, usuario y password de la BBDD en formato array
//        let passCoincidentes= []; // añado una nueva variable vacio para usuarios. 
//        for(let i=0; i<datoCuenta.length; i++){
//            // hago una comparacion si datoCuenta que viene de BBD = a "gio" entonces q muestre las contraseñas disponible
//            if (datoCuenta[i].usuario === "gio") {
//                console.log("bienvenido: "+ datoCuenta[i].usuario);
//                passCoincidentes.push(datoCuenta[i].password);// añado todo los "password:" de "datoCuenta" al "datoCuentaGuardado" del tipo array que estaba vacio.
//                
//            } else {
//            } 
//        } console.log(passCoincidentes); //muestras las contraseñas disponibles
//        
        //------------ 2° forma de hacer el array (usando metodo)---------------
        let  datoCuenta = Pruevas.baseDatos();// lo trae todo, usuario y password de la BBDD en formato array
        // tomo los datos atraves de filtes y comparo los usuarios y tomo los q coinciden. con el map solo le pido q me retorne la contraseña
        let passCoincidentes = datoCuenta
            .filter(function (usu) {
                return usu.usuario === "gio";
            })
            .find(function (usu){
                return usu.password;
            });            
            console.log("password de Gio:");
            console.log(passCoincidentes);
            
        //------------ 3° forma de hacer el array para edad(usando metodo)---------------   
        let edad = datoCuenta.filter(myFuntion);
        function myFuntion(element){
                return element.edad=== 3;
        }
           console.log("edad de Gio:");
            console.log(edad); 
    }
 
    static Find(){
        console.log("provando metodo Find...");
        
        // ************* 1 forma *************        
        let  datoCuenta = Pruevas.baseDatos();// lo trae todo, usuario y password de la BBDD en formato array
        // tomo los datos atraves de filtes y comparo los usuarios y terno los q coinciden. con el map solo le pido q me retorne la contraseña
        let usuCoincidentes = datoCuenta.find( usu => 
                console.log(usu.usuario)==="gio"                   
            );
            console.log("usuario ::");
            console.log(usuCoincidentes);
            
        // ************* 2 forma *************     
//        let usuCoincidentes2 = datoCuenta.find( usu => {
//            return (/gio/g).test(usu.usuario);
//        });
//            console.log("usuario2 ::");
//            console.log(usuCoincidentes2);
//   
    }
    
    static Some(){
            let  datoCuenta = Pruevas.baseDatos();
            let check1= datoCuenta.some( item =>{
                return item.usuario==="marco" ; // si es verdadero devuelve true, si es falso devuelve false
            } );
            console.log("SomeUsu ::");
            console.log(check1);
           let check2= datoCuenta.filter(todo).some(pass);
                       
            function todo(id){
                return id.usuario==="marco";
            }            
            function pass(id){
                return id.password==="123m" ; 
            }
            console.log("SomePass ::");
            console.log(check2); 
    }
    
    static Every(){ // devuelve true o false
            console.log("--estas en emtodo Everi---");

        let  datoCuenta = Pruevas.baseDatos();
        let checkEvery= datoCuenta.filter(usu).every(ed);
                function usu (id) {
                  return id.usuario === "gio"; // ingreso a array q cumplan esa condicion 
                }
                function ed (id) {
                  return id.edad >0; // solo dara true si y solo si toda la condion se cumpla
                }                   
        console.log("todosEdad>0 ?? ::"+checkEvery);            
                      
    ////////////////////////////// 
        var array1 = [1, 30, 39, 29, 10, 13];
        let total =array1.every(id);
        function id (currentValue) {
            return currentValue = 13;
        }
        console.log("alguno=13 ??::"+total);
        // expected output: true
        
    } 
    
    static ForEach(){
       let  datoCuenta = Pruevas.baseDatos();
       
       let a = datoCuenta.forEach(obj);
        function obj(id) {            
          console.log(id); // trare todo lo q esta en el array
        } 
        // console.log(a); no puedo hacerlo 
    }    
       
    static Reduce() {
            console.log("estas en metodo reduce");
            let  datoCuenta = Pruevas.baseDatos();

            let array = [
            { username: "Mapper", apellido: "Hernandes", edad: 22 },
            { username: "Finder", apellido: "bargas",  edad: 15 },
            { username: "Eaching", apellido: "aguilar",  edad: 10 }
            ];
        let sum = datoCuenta.filter(getUser).map(getEdad).reduce(getEdTotal);

            function getUser(id){
                return id.usuario === "sergio";
                // selecciona el vector q cumple con la condicion 
            }
            function getEdad(id){
                return id.edad * 2;
                // toma la edad y lo multiplica por 2 y devuelve en array
            } 
            function getEdTotal(total, id){
                return total + id ;
                // recuce lo comverte ese valor en objeto
            } 
            console.log(sum);
            //Output: 104
    }  
    
    static Push() {
            console.log("provando metodo push...");

            // El método push() añade uno o más elementos al final de un array y devuelve la nueva longitud del array
            let animales = ["gato", "perro", "rata"]; // armo mi array

            let total = animales.push("mono"); // agrego mono al array de animales
            console.log(total);  // imprime la cantidad = 4
            console.log(animales); // imprime el incluido: Array ["gato", "perro", "rata", "mono"]

            animales.push("Tigre", "Leon", "Gorila"); //agrego Tigre, leon y gorila al array
            console.log(animales);  // imprime el Array completo incluidos: Array ["gato", "perro", "rata", "mono", Tigre", "Leon", "Gorila"]
    }
    
    static mail(){ 
        console.log("estamos en mail..");
        //console.log("estas en mail.prueva");
        //Array.baseDatos();
        
        //Array.Map();
        //Array.Filter();
        //Array.Find();
        //Array.Some();
        //Array.Every();
        //Array.ForEach();        
        //Array.Reduce();
        //Array.Push();
       
    }
}






















/***************************** TEORIA Métodos de un Arreglo en Javascript: Filter, ForEach, Map y Find ******************************/
/*
 * 
Array.from()
Array.isArray()
Array.observe()
Array.of()
Array.prototype.concat()
Array.prototype.copyWithin()
Array.prototype.entries()
Array.prototype.every()
Array.prototype.fill()
Array.prototype.filter()
Array.prototype.find()
Array.prototype.findIndex()
Array.prototype.flat()
Array.prototype.flatMap()
Array.prototype.forEach()
Array.prototype.includes()
Array.prototype.indexOf()
Array.prototype.join()
Array.prototype.keys()
Array.prototype.lastIndexOf()
Array.prototype.map()
Array.prototype.pop()
Array.prototype.push()
Array.prototype.reduce()
Array.prototype.reduceRight()
Array.prototype.reverse()
Array.prototype.shift()
Array.prototype.slice()
Array.prototype.some()
Array.prototype.sort()
Array.prototype.splice()
Array.prototype.toLocaleString()
Array.prototype.toSource()
Array.prototype.toString()
Array.prototype.unshift()
Array.prototype.values()
Array.prototype[@@iterator]()
Array.unobserve()
get Array[@@species]
 * 
    En Javascript existen muchos métodos para arreglos muy importantes al momento de desarrollar lógica en alguna de nuestras aplicaciones, 
a continuación explico de forma breve el uso de Filter, ForEach, Map y Find.
Estos métodos poseen en común que reciben una función "callback" que permite 3 atributos, los cuales son: ELEMENTOS, INDICE y ARREGLO. 
Por lo general utilizamos el atributo ELEMENTO pero es bueno que conozcamos que podemos utilizar los otros atributos para algunas funciones mas específicas.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Filter-----------

    El método Filter crea un nuevo arreglo, el cual no busca modificar el valor de los elementos para el nuevo arreglo, 
únicamente retorna aquellos elementos que cumplan con las condiciones del filtro, los cuales serán almacenados en otro arreglo.
    
    let array = [
        { id: 1, username: "Mapper"},
        { id: 2, username: "Finder"},
        { id: 3, username: "Eaching"}
    ];
    let filtered = array.filter(function(element){
      return element.id > 2;
    }); 
        console.log(filtered);
    //Output: [{ id: "3", username: "Eaching"}]

                /////////// otra forma de escribir  /////////

    let filterId = array.filter(myFuntion);
        function myFuntion(element){
                return element.id === 3;
        }
        console.log(filterId);
        //Output: [{ id: "3", username: "Eaching"}]                    

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------MAP-----------

    El método Map tambien se encarga de crear un "nuevo arreglo" sin cambiar el anterior, al igual que el metodo Filter. 
Cada elemento es multiplicado por 1000 y a su vez se almacenan en un nuevo arreglo llamado miles.
    
    let array = [1, 2, 3, 4];
    let miles = array.map(function(element) {
      return element * 1000;
    });
    //Output: [1000, 2000, 3000, 4000]

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------FIND-----------

    Con la ayuda del método Find podemos encontrar el "Primer Valor" que cumpla con función que definimos,
en el siguiente ejemplo podemos observar que el arreglo posee 5 elementos numéricos,
posteriormente en la variable "valor" ejecutamos el método find y retornamos el elemento que sea mayor a 100 
obteniendo como resultado el primer valor del arreglo que cumple con la condicion.

    let array = [14, 123, 50, 20, 312];

    let valor = array.find(function(element) {
    return element > 100;
    });
    //Output: 123
                                
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    -----------ForEach-----------
  
    El método ForEach únicamente ejecuta la función callback (vulve a llamar de vuelta) que definimos, en el siguiente ejemplo
únicamente muestra como salida cada elemento del arreglo. Cabe destacar, 
que no retorna ningún valor ni tampoco un arreglo como los métodos anteriores.
  
    let array = [10, 20, 30, 40, 50];
    array.forEach(function(element) {
    console.log(element);
    });
    //Output: 10, 20, 30, 40, 50

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------
    El método every() verifica si "todos" los valores del vector "pasan en un determinado test" Y obtendrías un true o false.
    El método every() devuelve un booleano, true si "todos" los elementos en el arreglo pasan la condición 
implementada por la función dada y false si alguno no la cumple
   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Some-----------
    Contrario de every(), el método some() verifica si "alguno" de los valores del vector "pasa un determinado test".
    El método some() comprueba si al menos "un" elemento del array cumple con la condición implementada por la función proporcionada

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    -----------Reduce-----------
    El método reduce() ejecuta una función en cada uno de los elementos del vector para “reducirlo” a un valor único. 
Es importante tener en cuenta que reduce() funciona de izquierda a derecha y no “reduce” el vector original.
    
    let array = [
        { username: "Mapper", apellido: "Hernandes", edad=20 },
        { username: "Finder", apellido: "bargas",  edad=10 },
        { username: "Eaching", apellido: "aguilar",  edad=40 }
    ];
    let sum = array.filter(getUser).map(getEdad).reduce(getEdTotal)
        console.log(sum);

    funtion.getUser(id){
      return id.username === "Finder";  //toma el primer array     
    }
    funtion.getEdad(id){
      return id.edad * 2; // toma un valor del array y opera
    }
    funtion.getEdTotal(total, id){
      return total + id ; // retorna en objeto
    }
    
    );
    //Output: [{ id: "3", username: "Eaching"}]

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    -----------forEach-----------
    forEach() llama y ejecuta una función una vez por cada elemento del vector. Comparando con map() o reduce(), 
forEach() es ideal cuando lo que quieres no es cambiar los datos de tu vector, si no guardarlos en alguna base de datos. 
forEach() siempre devuelve el valor undefined y no es encadenable. Lo podrías ejecutar de la siguiente forma:

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------


    

  */